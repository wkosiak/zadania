#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
	char kategoria;
	int godziny, nadgodziny, decyzja;
	int katA = 15;
	int katB = 25;
	int katC = 30;
	int katD = 35;
	double brutto, netto;

	do {

		cout << "Podaj kategorie zaszeregowania (A, B, C lub D)" << endl;
		cin >> kategoria;
		cout << "Podaj liczbe godzin" << endl;
		cin >> godziny;
		nadgodziny = 0;
		if (godziny > 40) {
			nadgodziny = godziny - 40;
		}

		switch (kategoria) {
		case('a'):
			
			brutto = (godziny*katA) + nadgodziny*katA;
			if (brutto < 700) {
				netto = 0.85 * brutto;
			}
			else if (700 < brutto < 1200) {
				netto = 0.80 * brutto;
			}
			else {
				netto = 0.75 * brutto;
			}
			cout << "Zarobki brutto wynosza: " << brutto << endl;
			cout << "Zarobki netto wynosza: " << netto << endl;
			_getch();
			cout << "Nacisnij 1 aby ponownie uruchomic program" << endl;
			cin >> decyzja;
			break;
			

		case('b'):
			brutto = godziny*katB + nadgodziny*katB;
			if (brutto < 700) {
				netto = 0.85 * brutto;
			}
			else if (700 < brutto < 1200) {
				netto = 0.80 * brutto;
			}
			else {
				netto = 0.75 * brutto;
			}
			cout << "Zarobki brutto wynosza: " << brutto << endl;
			cout << "Zarobki netto wynosza: " << netto << endl;
			_getch();
			cout << "Nacisnij 1 aby ponownie uruchomic program" << endl;
			cin >> decyzja;
			break;

		case('c'):
			brutto = godziny*katC + nadgodziny*katC;
			if (brutto < 700) {
				netto = 0.85 * brutto;
			}
			else if (700 < brutto < 1200) {
				netto = 0.80 * brutto;
			}
			else {
				netto = 0.75 * brutto;
			}
			cout << "Zarobki brutto wynosza: " << brutto << endl;
			cout << "Zarobki netto wynosza: " << netto << endl;
			_getch();
			cout << "Nacisnij 1 aby ponownie uruchomic program" << endl;
			cin >> decyzja;
			break;

		case('d'):
			brutto = godziny*katD + nadgodziny*katD;
			if (brutto < 700) {
				netto = 0.85 * brutto;
			}
			else if (700 < brutto < 1200) {
				netto = 0.80 * brutto;
			}
			else {
				netto = 0.75 * brutto;
			}
			cout << "Zarobki brutto wynosza: " << brutto << endl;
			cout << "Zarobki netto wynosza: " << netto << endl;
			_getch();
			cout << "Nacisnij 1 aby ponownie uruchomic program" << endl;
			cin >> decyzja;
			break;

		}

		system("cls");

	} while (decyzja == 1); 

}